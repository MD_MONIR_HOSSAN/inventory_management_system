from django.contrib import admin
from django.urls import path
from . import views




urlpatterns = [
    path('', views.index, name="index" ),
    path('all_laptops/', views.all_laptops, name='all_laptops'),
    path('all_mobiles/', views.all_mobiles, name='all_mobiles'),
    path('add_laptops/', views.add_laptops, name='add_laptops'),
    path('add_mobiles/', views.add_mobiles, name='add_mobiles'),
    path('edit_laptop/<id>', views.edit_laptop, name='edit_laptop'),
    path('edit_mobile/<id>', views.edit_mobile, name='edit_mobile'),
    path('delete_laptop/<id>', views.delete_laptop, name='delete_laptop'),
    path('delete_mobile/<id>', views.delete_mobile, name='delete_mobile'),

    path('get_all_laptops/', views.get_all_laptops, name='get_all_laptops'),
]