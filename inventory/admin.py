from django.contrib import admin
from .models import Laptops, Mobiles

# Register your models here.

class Laptop(admin.ModelAdmin):
	list_display = ('id', 'type', 'price', 'status', 'issues')

class Mobile(admin.ModelAdmin):
	list_display = ('id', 'type', 'price', 'status', 'issues')


admin.site.register(Laptops, Laptop)
admin.site.register(Mobiles, Mobile)