from django.db import models

# Create your models here.

class Products(models.Model):

    type = models.CharField(max_length=200, blank=False)
    price = models.IntegerField()

    choices = (
        ('AVAILABLE', 'Item is ready to be purchased'),
        ('SOLD', 'Item is already purchased'),
        ('RESTOCKING', 'Item will restock in few days')
    )

    status = models.CharField(max_length=10, choices=choices, default='SOLD')
    issues = models.CharField(max_length=50, default="No Issues")

    class Meta:
        abstract = True

    def __str__(self):
        return f'Type: {self.type} , Price: {self.price}'

class Laptops(Products):
    pass

class Mobiles(Products):
    pass
