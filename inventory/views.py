from django.shortcuts import render, redirect, get_object_or_404
from .models import Laptops, Mobiles
from .forms import  LaptopForm, MobileForm
from django.http import HttpResponse
import requests

# Create your views here.

def index(request):
	return render(request, "inventory/index.html")


def all_laptops(request):
    products = Laptops.objects.all()
    context = {
        'products': products,
        'header': 'Laptops',
    }
    return render(request, 'inventory/index.html', context)

def all_mobiles(request):
    products = Mobiles.objects.all()
    context = {
        'products': products,
        'header': 'Mobiles',
    }
    return render(request, 'inventory/index.html', context)

def add_products(request, form_cls, red_path):
    if request.method == "POST":
        form = form_cls(request.POST)
        if form.is_valid():
        	form.save()
        	return redirect(red_path)
    else:
        form = form_cls()
        return render(request, 'inventory/add_new.html', {'form' : form})

def add_laptops(request):
	return add_products(request, LaptopForm, 'all_laptops')

def add_mobiles(request):
	return add_products(request, MobileForm, 'all_mobiles')


def edit_product(request, id, model, form_cls, red_path):
    products = get_object_or_404(model, id=id)

    if request.method == "POST":
        form = form_cls(request.POST, instance=products)
        if form.is_valid():
            form.save()
            return redirect(red_path)
    else:
        form = form_cls(instance=products)

        return render(request, 'inventory/edit_products.html', {'form': form})



def edit_laptop(request, id):
    return edit_product(request, id, Laptops, LaptopForm, 'all_laptops')


def edit_mobile(request, id):
    return edit_product(request, id, Mobiles, MobileForm, 'all_mobiles')


def delete_laptop(request, id):

    Laptops.objects.filter(id=id).delete()

    return redirect('all_laptops')

def delete_mobile(request, id):

    Mobiles.objects.filter(id=id).delete()

    return redirect('all_mobiles')


def get_all_laptops(request):
    url = 'https://inv-m-system.herokuapp.com/api/get_laptops/'

    response = requests.get(url)
    j = response.text
    return HttpResponse(j)





