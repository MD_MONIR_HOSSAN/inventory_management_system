import json
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from inventory.models import Laptops, Mobiles

def get_laptops(request):
    if request.method == 'GET':
        try:
            laptops = Laptops.objects.all()
            print(laptops)
            output = []
            for item in laptops:
                response = json.dumps([{ 
                    'Type': item.type, 
                    'Price': item.price,
                    'Status' : item.status,
                    'Issues' : item.issues,
                    }])
                output.append(response)
        except:
            response = json.dumps([{ 'Error': 'No laptop found'}])
            output.append(response)
    return HttpResponse(output, content_type='text/json')

def get_mobiles(request):
    if request.method == 'GET':
        try:
            mobiles = Mobiles.objects.all()
            output = []
            for item in mobiles:
                response = json.dumps({ 
                    'Type': item.type, 
                    'Price': item.price,
                    'Status' : item.status,
                    'Issues' : item.issues,
                    })
                output.append(response)
        except:
            response = json.dumps([{ 'Error': 'No laptop found'}])
            output.append(response)
    return HttpResponse(output, content_type='text/json')


def get_laptops_type(request, id):
    if request.method == 'GET':
        try:
            laptops = Laptops.objects.get(id=id)
            print(laptops)
            # output = []
            response = json.dumps({ 
                'Type': laptops.type, 
                'Price': laptops.price,
                'Status' : laptops.status,
                'Issues' : laptops.issues,
                })
        except:
            response = json.dumps([{ 'Error': 'No laptop found'}])
    return HttpResponse(response, content_type='text/json')

def get_mobiles_type(request, id):
    if request.method == 'GET':
        try:
            mobiles = Mobiles.objects.get(id=id)
            print(mobiles)
            response = json.dumps({ 
                'Type': mobiles.type, 
                'Price': mobiles.price,
                'Status' : mobiles.status,
                'Issues' : mobiles.issues,
                })
        except:
            response = json.dumps([{ 'Error': 'No mobile found'}])

    return HttpResponse(response, content_type='text/json')


