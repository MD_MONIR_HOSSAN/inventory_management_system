from django.urls import path
from . import views




urlpatterns = [
    path('get_laptops/', views.get_laptops, name='get_laptops'),
    path('get_mobiles/', views.get_mobiles, name='get_mobiles'),
    path('get_laptops_type/<int:id>', views.get_laptops_type, name='get_laptops_type'),
    path('get_mobiles_type/<int:id>', views.get_mobiles_type, name='get_mobiles_type'),
]